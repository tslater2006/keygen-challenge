package com.me.keygen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.me.keygen.helpers.AttemptReporter;
import com.me.keygen.verifiers.AppVerifier;
import com.me.keygen.verifiers.KeyVerifier;
import com.me.keygen.verifiers.challenge.Challenge1Verifier;
import com.me.keygen.verifiers.challenge.Challenge2Verifier;
import com.me.keygen.verifiers.challenge.Challenge3Verifier;
import com.me.keygen.verifiers.challenge.Challenge4Verifier;
import com.me.keygen.verifiers.challenge.Challenge5Verifier;

public class MainActivity extends Activity implements AdapterView.OnItemSelectedListener
{
    Button submit = null;
    EditText name = null;
    EditText serial = null;
    TextView version = null;
    AlertDialog success = null;
    AlertDialog failure = null;
    
    Spinner levelSelect = null;
    
    int currentChallenge = 1;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Log.i("HASH", AppVerifier.getJarHash(this));
        setContentView(R.layout.main);
        
        submit = (Button)findViewById(R.id.btnSubmit);
        name = (EditText)findViewById(R.id.txtName);
        serial = (EditText)findViewById(R.id.txtSerial);
        version = (TextView)findViewById(R.id.txtBuildVersion);
        levelSelect = (Spinner)findViewById(R.id.levelSelect);
        try {
            PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            int versionNumber = pinfo.versionCode;
            String versionName = pinfo.versionName;
            version.setText("Build Version: " + versionName);
        }catch(Exception e){}
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
        R.array.levels, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        levelSelect.setOnItemSelectedListener(this);
        // Apply the adapter to the spinner
        levelSelect.setAdapter(adapter);
        
        submit.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                MainActivity.this.validateSerial();
            }
        });
        
        // build success dialog
        success = new Builder(this)
                .setTitle("Congratulations")
                .setMessage("Congratulations on figuring out the algorithm.")
                .setPositiveButton("OK", new DismissDialogListener())
                .create();
        
        // build failure dialog
        failure = new Builder(this)
                .setTitle("Wrong Answer")
                .setMessage("I'm sorry but that is incorrect, please try again.")
                .setPositiveButton("OK", new DismissDialogListener())
                .create();
                
        
        // build and show intro dialog
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Welcome to Keygen Challenge")
                .setMessage("Select a difficulty level and try to figure out the correct serial number!")
                .setPositiveButton("OK",new DismissDialogListener());
        b.create().show();
    }
    
    
    private void validateSerial()
    {
        String name = this.name.getText().toString();
        String serial = this.serial.getText().toString();
        KeyVerifier kv = getVerifierForChallenge(currentChallenge);
        if (kv == null)
        {
            Toast.makeText(this,"No Verifier Present For This Level",Toast.LENGTH_SHORT).show();
            return;
        }
        boolean valid = kv.isValid(name, serial);
        String result = null;
        if (valid)
        {
            success.show();
            result = "Correct";
        }else
        {
            failure.show();
            result = "Incorrect";
        }
        
        String formURL = "https://docs.google.com/spreadsheet/formResponse?formkey=" + getString(R.string.form_key) + "&ifq";
        String hash = AppVerifier.getJarHash(this);        
        
        
        new AttemptReporter(this, formURL, hash, result, currentChallenge).execute(new Object[0]) ;
        
        
    }
    
    private KeyVerifier getVerifierForChallenge(int challengeId)
    {
        switch (challengeId)
        {
            case 1:
                return new Challenge1Verifier();
            case 2:
                return new Challenge2Verifier();
            case 3:
                return new Challenge3Verifier();
            case 4:
                return new Challenge4Verifier();
            case 5:
                TelephonyManager tm = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
                return new Challenge5Verifier(tm.getDeviceId());
            default:
                return null;
        }
    }

    public void onItemSelected(AdapterView<?> av, View view, int i, long l) {
        currentChallenge = i + 1;
        name.setText("");
        serial.setText("");
    }

    public void onNothingSelected(AdapterView<?> av) {
        currentChallenge = 1;
    }
    
    class DismissDialogListener implements DialogInterface.OnClickListener
    {

        public void onClick(DialogInterface di, int i) {
            di.dismiss();
        }
        
    }
}

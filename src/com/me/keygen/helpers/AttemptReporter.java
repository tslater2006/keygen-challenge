/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.keygen.helpers;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

/**
 *
 * @author HOME
 */
public class AttemptReporter extends AsyncTask {
    
    String hash = null, result = null,serial = null,formURL = null;
    
    int currentChallenge;
    Context ctx;
    public AttemptReporter(Context ctx, String url,String h, String r, int c)
    {
        formURL = url;
        hash = h;
        result = r;
        currentChallenge = c;
        this.ctx = ctx;
    }
    
    @Override
    public void onPostExecute(Object o)
    {
        //Toast.makeText(ctx,"Posted Results",Toast.LENGTH_SHORT).show();
    }
    
    
    public void postAttempt() {    
    
    // Create a new HttpClient and Post Header
    HttpClient httpclient = new DefaultHttpClient();
    HttpPost httppost = new HttpPost(formURL);

    try {
        // Add your data
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("entry.0.single", "" + currentChallenge));
        nameValuePairs.add(new BasicNameValuePair("entry.1.single", result));
        nameValuePairs.add(new BasicNameValuePair("entry.2.single", hash));
        nameValuePairs.add(new BasicNameValuePair("pageNumber", "0"));
        nameValuePairs.add(new BasicNameValuePair("backupCache", ""));
        nameValuePairs.add(new BasicNameValuePair("submit", "Submit"));
        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

        // Execute HTTP Post Request
        HttpResponse response = httpclient.execute(httppost);
        
    } catch (ClientProtocolException e) {
        // TODO Auto-generated catch block
    } catch (IOException e) {
        // TODO Auto-generated catch block
    }
} 

    @Override
    protected Object doInBackground(Object... paramss) {
        postAttempt();
        return null;
    }
}

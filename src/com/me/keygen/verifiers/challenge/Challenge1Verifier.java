/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.keygen.verifiers.challenge;

import com.me.keygen.verifiers.KeyVerifier;

/**
 *
 * @author HOME
 */
public class Challenge1Verifier implements KeyVerifier{

    public boolean isValid(String name, String serial) {
        int answer = 0;
        
        for (int x = 0; x < name.length(); x++)
        {    
            char current = name.charAt(x);
            answer += current * current;
            answer ^= current;
        }
        try
        {
            int numericSerial = Integer.parseInt(serial);
            if (numericSerial == answer)
            {
                return true;
            }
        }catch(Exception e) {}
        return false;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.keygen.verifiers.challenge;

import android.util.Log;
import com.me.keygen.verifiers.KeyVerifier;
import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import java.security.MessageDigest;

/**
 *
 * @author HOME
 */
public class Challenge3Verifier implements KeyVerifier{
    // Xilisoft Algorithm :)
    
    String secretKey = "KeygenChallengeNumber3";
    byte[] secretBytes = secretKey.getBytes(Charset.forName("US-ASCII"));

    public boolean isValid(String name, String serial) {
        
        String[] parts = serial.split("-");
        if (parts.length != 8) 
        {
            return false;
        }
        
        for (int x = 0; x < parts.length; x++)
        {
            if (parts[x].matches("[0-9A-F][0-9A-F][0-9A-F][0-9A-F]") == false)
            {
                return false;
            }
        }
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        
        baos.write(0x31);
        
        for (int x = 0; x < secretBytes.length; x +=2)
        {
            baos.write(secretBytes[x]);
            baos.write(x+1);
        }
        
        for (int x = 1; x < secretBytes.length; x+= 2)
        {
            baos.write(secretBytes[x]);
            baos.write(x+1);
        }
        
        baos.write(0x30);
        baos.write(0x30);
        
        for (int x = 0; x < parts.length / 2; x++)
        {
            try
            {
                baos.write(parts[x].getBytes(Charset.forName("US_ASCII")));
                baos.write(0x2d);
            }catch (Exception e){return false;}
        }
        try
        {
            baos.write(secretBytes);
        } catch(Exception e){return false;}
        
        
        byte[] result = new byte[32];
        
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(baos.toByteArray());
            result = md.digest();
        }catch(Exception e){return false;}
        
        String foo = bytesToHex(result).toUpperCase();
        String lastHalf = serial.substring(serial.length()/2);
        lastHalf = lastHalf.replaceAll("-", "");
        
        for (int x = 0; x < foo.length(); x += 2)
        {
            if (foo.charAt(x) != lastHalf.charAt(x / 2)) 
            {
                return false;
            }
        }
        
        return true;
        
    }
    
    
    public static String bytesToHex(byte[] bytes) {
        final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for ( int j = 0; j < bytes.length; j++ ) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}

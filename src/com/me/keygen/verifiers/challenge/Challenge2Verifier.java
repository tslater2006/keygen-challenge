/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.keygen.verifiers.challenge;

import com.me.keygen.verifiers.KeyVerifier;

/**
 *
 * @author HOME
 */
public class Challenge2Verifier implements KeyVerifier{

    public boolean isValid(String name, String serial) {
        if (name.length() < 4) 
        {
            return false;
        }
        
        name = name.toUpperCase();
        long nameSum = 0;
        for (int x = 0; x < name.length(); x++)
        {
            nameSum += name.charAt(x);
            nameSum *= 3;
            nameSum -= 64;
        }
        
        String sumString = Long.toString(nameSum);
        int finalSum = 0;
        for (int x = 0; x < sumString.length(); x++)
        {
            finalSum += (sumString.charAt(x) - 48);
        }
        
        for (int x = 0; x < serial.length(); x++)
        {
            finalSum -= serial.charAt(x) - 64;
        }
        
        return finalSum == 0;
   }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.keygen.verifiers.challenge;

import com.me.keygen.verifiers.KeyVerifier;
import java.nio.ByteBuffer;

/**
 *
 * @author HOME
 */
public class Challenge5Verifier implements KeyVerifier
{
    String id;
    int[] keys = new int[3];
    int[][] matrix;

    public Challenge5Verifier(String Id)
    {
        id = Id;
    }
    public static void main(String[] args) 
    {
        Challenge5Verifier v = new Challenge5Verifier("mwahahaha");
        System.out.println(v.isValid("Good", "Luck"));
    }

    public boolean isValid(String name, String serial) 
    {
        byte[] b = makeKey(name, id);

        RandomGen calc;
        matrix = new int[b.length][b.length];
        int sum = 0;
        for (int x = 0; x < b.length; x++) 
        {
            sum += (b[x] & 0xFF);
        }

        calc = new RandomGen(sum * (name.length() + serial.length() + id.length()));
        for (int x = 0; x < b.length; x++) 
        {

            calc.getNext();
            for (int y = 0; y < b.length; y++) 
            {
                matrix[x][y] = calc.getNext();
            }
        }
        normalizeMatrix(matrix);
        long val = mst(matrix);
        System.out.println(val);
        byte[] input = makeKey(name, serial);
        byte[] check = makeKey(name, Long.toString(val));

        for (int x = 0; x < input.length; x++) 
        {
            if (input[x] != check[x]) 
            {
                return false;
            }
        }

        return true;
    }

    private long mst(int[][] b) 
    {
        long totalWeight = 0;
        long curSmallest = 0;
        int smallestRow = -1;
        int columnCount = b.length;
        int rowCount = columnCount;
        boolean[] visited = new boolean[columnCount];

        // select first column;
        int curColumn = 0;

        visited[curColumn] = true;

        while (getVisitedCount(visited) < columnCount) 
        {
            smallestRow = -1;
            // search all visited columns;
            for (int x = 0; x < columnCount; x++) 
            {
                // if node was selected, search column
                if (visited[x]) 
                {
                    for (int y = 0; y < rowCount; y++) 
                    {
                        long curValue = b[x][y] & 0xFFFFFF;
                        if ((curValue < curSmallest && visited[y] == false) || smallestRow == -1) 
                        {
                            curSmallest = curValue;
                            smallestRow = y;
                        }
                    }
                }
            }
            totalWeight += curSmallest;
            visited[smallestRow] = true;
        }
        return totalWeight;
    }

    private int getVisitedCount(boolean[] b) 
    {
        int count = 0;
        for (int x = 0; x < b.length; x++) 
        {
            count += b[x] ? 1 : 0;
        }
        return count;
    }

    private void normalizeMatrix(int[][] b) 
    {
        for (int x = 0; x < b.length; x++) 
        {
            for (int y = 0; y < b[x].length; y++) 
            {
                b[x][y] = (b[x][y] + b[y][x]) / 2;
                b[y][x] = (b[x][y] + b[y][x]) / 2;
                b[x][x] = -1;
            }
        }
    }

    private byte[] makeKey(String name, String deviceId) 
    {
        while (name.length() < 12) 
        {
            name += name;
        }
        while (deviceId.length() < 40) 
        {
            deviceId += deviceId;
        }
        byte[] bytes = name.getBytes();
        ByteBuffer buff = ByteBuffer.wrap(bytes);
        for (int x = 0; x < 3; x++) 
        {
            keys[x] = buff.getInt();
        }

        bytes = deviceId.getBytes();
        for (int x = 0; x < bytes.length; x++) 
        {
            bytes[x] = cryptByte(bytes[x]);
        }
        return bytes;
    }

    private byte cryptByte(byte b) 
    {
        int EBP_4 = 0;
        int EBP_C = keys[1] % 2;
        int EBP_8 = keys[2] % 2;

        for (int x = 0; x < 8; x++) 
        {
            if (keys[0] % 2 == 0) 
            {
                keys[0] = keys[0] >> 1;
                keys[0] = keys[0] & 0x7FFFFFFF;
                if (keys[2] % 2 == 0) 
                {
                    keys[2] = keys[2] >> 1;
                    keys[2] = keys[2] & 0x0FFFFFFF;
                    EBP_8 = 0;
                } else 
                {
                    keys[2] = keys[2] ^ 0x10000002;
                    keys[2] = keys[2] >> 1;
                    keys[2] = keys[2] | 0xF0000000;
                    EBP_8 = 1;
                }
            } else 
            {
                keys[0] = keys[0] & 0x80000000;
                keys[0] = keys[0] ^ 0x62;
                keys[0] = keys[0] >> 1;
                keys[0] = keys[0] | 0x80000000;

                if (keys[1] % 2 == 0) 
                {
                    keys[1] = keys[1] >> 1;
                    keys[1] = keys[1] & 0x3FFFFFFF;
                    EBP_C = 0;
                } else 
                {
                    keys[1] = keys[1] ^ 0x40000020;
                    keys[1] = keys[1] >> 1;
                    keys[1] = keys[1] | 0xC0000000;
                    EBP_C = 1;
                }
            }

            EBP_4 = EBP_4 * 2;
            EBP_4 = EBP_4 & 65535;
            int temp = EBP_C ^ EBP_8;
            EBP_4 = EBP_4 | temp;
        }
        return (byte) (b ^ EBP_4);
    }
}

class RandomGen 
{

    int ConstantValue = 0x1DF5E0D;
    private int lastValue = 0;
    private int seed = 0;

    public RandomGen(int seed) 
    {
        this.seed = seed;
    }

    public int getNext() 
    {
        int Product1 = 0;
        int Product2 = 0;
        int Product3 = 0;
        int Sum = 0;
        int finalValue = 0;

        Product1 = (lastValue / 0x2710) * (ConstantValue % 0x2710);
        Product2 = (ConstantValue / 0x2710) * (lastValue % 0x2710);
        Product3 = ((ConstantValue % 0x2710) * (lastValue % 0x2710));

        Sum = Product3 + (((Product1 + Product2) % 0x2710) * 0x2710);

        lastValue = (Sum % 0x5F5E100) + 1;

        long tempVal = 0;
        long multResult = 0;
        long TempResult = 0;
        tempVal = lastValue % 0x5f5e100;
        tempVal /= 0x2710;
        multResult = (tempVal * seed);
        multResult = 0xFFFFFFFFL & multResult;

        TempResult = ((multResult) / 0x2710);

        if (TempResult != 0) 
        {
            if ((TempResult % 4) != 0) 
            {
                finalValue = (int) TempResult >> 2;
                finalValue *= 4;
                finalValue += 4;
            } else 
            {
                finalValue = (int) TempResult;
            }
        } else 
        {
            finalValue = 0x5f5e100;
        }

        return finalValue;
    }
}

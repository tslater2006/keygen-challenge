/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.keygen.verifiers.challenge;

import com.me.keygen.verifiers.KeyVerifier;
import java.math.BigInteger;

public class Challenge4Verifier implements KeyVerifier
{
    public int h[] = new int[]{2, 2,-2,-2,1, 1,-1,-1};
    public int v[] = new int[]  {1,-1, 1,-1,2,-2, 2,-2};
    public int matrixSize;// = 58;
    public int matrixCount[][];
    public int matrix[][];
    
    public int locationX = 0;
    public int locationY = 0;
    public int steps = 0;

    public StringBuilder nameBuffer = new StringBuilder();
    public int nameOffset = 0;
    
    public void main(String args[])
    {
     //       System.out.println(verify("Nieylana","55700202085603606528"));
    }

    public boolean isValid(String name, String serial)
    {
        nameBuffer = new StringBuilder();
        int counter = 0;
        name = name.toLowerCase();
        while (nameBuffer.length() < 7000 )
        {
            nameBuffer.append(name);
            counter++;
        }
        matrixSize = (counter % 49) + 9;
        initMatrix();
        populateMatrix();

        BigInteger[] bigInts = new BigInteger[matrixSize];
        for (int x = 0; x < matrixSize; x++)
        {
            bigInts[x] = new BigInteger(intArrayToHexString(matrix[x]),16);
            if (x > 1)
            {
                bigInts[x] = bigInts[x].multiply(bigInts[x-2]);
            }
            if (x > 0)
            {
                bigInts[x] = bigInts[x].xor(bigInts[x-1]);
            }
        }
        BigInteger bigOne = bigInts[matrixSize -1].mod(new BigInteger("100000000000000000000"));
        bigOne = new BigInteger(bigOne.toString());	
        try 
        {	
            BigInteger serialInt = new BigInteger(serial);
            System.out.println(bigOne.toString());
            return bigOne.xor(serialInt).equals(new BigInteger("0"));
        }catch(Exception e){return false;}


    }

    private static String intArrayToHexString(int[] ints)
    {
        StringBuilder sb = new StringBuilder();
        for(int x = 0; x < ints.length; x++)
        {
            sb.append(String.format("%04X",ints[x]));
        }
        return sb.toString();
    }

    private void initMatrix()
    {
    	matrix = new int[matrixSize][matrixSize];
    	matrixCount = new int[matrixSize][matrixSize];
    	for (int x = 0; x < matrixSize; x++) 
        {
            for (int y = 0; y < matrixSize; y++) 
            {
                matrix[x][y] = 0;
            }
        }

        for (int x = 0; x < matrixSize; x++)
        {
            for (int y = 0; y < matrixSize; y++)
            {
                for (int z = 0; z < 8; z++)
                {
                    if (isValidLocation(x,y,z))
                    {
                        matrixCount[x][y]++;
                    }
                }
            }
        }
    }

    private int populateMatrix()
    {
        initMatrix();
        int startX = nameBuffer.charAt(nameOffset++) % matrixSize;
        int startY = nameBuffer.charAt(nameOffset++) % matrixSize;
        setLocation(startX,startY);


        while (getNextLocation() != -1)
        {
            int bestLoc = getNextLocation();
            setLocation(locationX + h[bestLoc], locationY + v[bestLoc]);
        }
        return steps;
    }

    private boolean isValidLocation (int x, int y, int move)
    {
        if ((x + h[move] < matrixSize && x+h[move] >=0) && (y + v[move] < matrixSize && y + v[move] >=0)) 
        {
            return true;
        }
        
        return false;
    }

    private boolean getTieBreaker()
    {
    	return (nameBuffer.charAt(nameOffset++) - 96) % 2 == 1;
    }

    private int getNextLocation()
    {
        int bestMove = -1;
        int smallestCount = 9;
        
        for (int z = 0; z < 8; z++)
        {
            if (isValidLocation(locationX,locationY,z))
            {
                // make sure it hasn't been landed on yet!
                
                if (matrix[locationX + h[z]][locationY + v[z]] == 0)
                {
                    int moveCount = matrixCount[locationX + h[z]][locationY + v[z]];
                    
                    if (moveCount < smallestCount)
                    {
                        smallestCount = moveCount;
                        bestMove = z;
                    } else if (moveCount == smallestCount)
                    {
                        if (getTieBreaker())
                        {
                            bestMove = z;
                        }
                    }
                }
                else
                {
                }
            }
        }
        return bestMove;
    }

    private void setLocation(int newX, int newY)
    {
        steps++;
        for (int z = 0; z < 8; z++)
        {
            if (isValidLocation(newX,newY,z))
            {
                matrixCount[newX + h[z]][newY + v[z]]--;
            }
        }
        matrix[newX][newY] = steps;
        locationX = newX;
        locationY = newY;
        
    }
}

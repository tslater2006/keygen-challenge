/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.keygen.verifiers;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.Enumeration;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 *
 * @author HOME
 */
public class AppVerifier {
    private static String getSha1(Context ctx)
    {
        try {
            // Get Package Name
            String packageName = ctx.getPackageName();

            // Get classes.dex file signature
            ApplicationInfo ai = ctx.getApplicationInfo();
            String source = ai.sourceDir;

            JarFile jar = new JarFile(source);
            java.util.jar.Manifest mf = jar.getManifest();

            Map<String, Attributes> map = mf.getEntries();

            Attributes a = map.get("classes.dex");
            String sha1 = (String)a.getValue("SHA1-Digest");
            return sha1;
        } catch(Exception e){}
        return "";
    }
    
    private static byte[] streamToArray(InputStream is)
    {
        try {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[16384];

            while ((nRead = is.read(data, 0, data.length)) != -1) {
              buffer.write(data, 0, nRead);
            }

            buffer.flush();

            return buffer.toByteArray();
        }catch (Exception e){}
        return new byte[0];
    }
    
    public static String getJarHash(Context ctx)
    {
        try {
            String packageName = ctx.getPackageName();
            // Get classes.dex file signature
            ApplicationInfo ai = ctx.getApplicationInfo();
            String source = ai.sourceDir;

            JarFile jar = new JarFile(source);
            MessageDigest md = MessageDigest.getInstance("md5");
            
            for (Enumeration<JarEntry> em1 = jar.entries(); em1.hasMoreElements();) {
                JarEntry curEntry = em1.nextElement();
                InputStream is = jar.getInputStream(curEntry);
                md.update(streamToArray(is));
            }
            
            StringBuilder hexString = new StringBuilder();
            byte[] hash = md.digest();
            
            for (int i = 0; i < hash.length; i++) {
                if ((0xff & hash[i]) < 0x10) {
                    hexString.append("0"
                        + Integer.toHexString((0xFF & hash[i])));
                } else {
                hexString.append(Integer.toHexString(0xFF & hash[i]));
                }
            }
            return hexString.toString();
        }catch(Exception e){}
            return getSha1(ctx);
    }   
}
